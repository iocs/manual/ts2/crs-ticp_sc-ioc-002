# Startup for CrS-TICP:SC-IOC-002

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/crs_ticp_cryo_plc_02.iocsh", "DBDIR=$(E3_CMD_TOP)/db/, MODVERSION=$(IOCVERSION=)")

